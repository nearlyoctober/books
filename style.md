## General formatting standards

* No spaces around em dashes
* `&nbsp;`s around `&hellip;`s

## Vim commands for formatting curly quotes

* Curly quotes everything (`\qc`)
* Remove all fancy quotes between angle brackets
  * `%s/\(<[^>]*\)\(”\|“\)\([^>]*>\)/\1"\3/g`
    * Run this many until there are no longer any occurrences (6 times?)
  * Can also check with this: `/=(”\|“)`
* `>”` replace this crap
  * `%s/>”/>“/g`
* And then find instances of two same-direction quotes in a row (without an opposite-direction quote in between)
  * `/“\([^”\|“]*\)“`
  * `/”\([^”\|“]*\)”`
* Verify all back-single-quotes
  * `/‘`
  * Really we're just looking for this: `/\S‘`
* Remove this crap: `/’“` & `/”\w`

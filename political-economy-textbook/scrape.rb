# Runs on Ruby 2.3
#
# `nokogiri` gem must be installed

require 'nokogiri'
require 'net/http'
require 'uri'

def open(url)
  Net::HTTP.get(URI.parse(url))
end

class NokogiriHandler
  def inner_text(node_set, regex)
    node_set.find_all { |node| node.inner_text =~ /#{regex}/ }
  end
end

def titleize(str)
  str = str.capitalize
  words_no_cap = %w(a an and at but by for in nor of on or so the to up yet)
  phrase = str.split(' ').map { |word|
    words_no_cap.include?(word) ? word : word.capitalize
  }.join(' ')
  phrase
end

def extract_notes(doc)
  notes = []

  notes_node = doc.at_css('h4:inner_text("(?i)\ANotes\z")', NokogiriHandler.new)
  if notes_node
    sib = notes_node
    loop do
      sib = sib.next_element
      break if sib.name == 'hr'
      anchor = sib.at_css('a')
      if anchor
        notes << []
        anchor.remove
      end
      dup = sib.dup
      dup.remove_attribute('class')
      dup.name = 'span'
      notes.last << dup
    end
  end

  notes
end

def extract_chapter_title(doc)
  sib = doc.at_css('hr')
  fallback = nil
  loop do
    sib = sib.next_element
    if sib.name == 'h3'
      fallback = sib.inner_text
    elsif sib.name == 'h4'
      _, num, title = *sib.inner_text.match(/\ACHAPTER ([A-Z]+) : (.+)(\s*)\z/)
      return [num, titleize(title)]
    elsif sib.name == 'hr'
      return fallback ? [nil, titleize(fallback)] : nil
    end
  end
end

def extract_body(doc)
  # Pre-process doc...
  doc.css('p').each do |element|
    if element.attribute('class').to_s.include?('quote')
      element.set_attribute('class', 'quote')
    elsif element.attribute('class').to_s == 'fst'
      element.set_attribute('class', 'no-indent')
    else
      element.remove_attribute('class')
    end
  end

  doc.css('img').each do |element|
    element.remove_attribute('alt')
    element.remove_attribute('width')
    element.remove_attribute('height')
    element.parent.replace(element) if element.parent.name == 'p'
  end

  doc.css('table').each do |element|
    element.remove_attribute('width')
    element.remove_attribute('border')
    element.remove_attribute('align')
    element.remove_attribute('cellpadding')
  end

  doc.css('td').each do |element|
    element.remove_attribute('class')
  end

  doc.css('h4').each do |element|
    if element.content == 'BRIEF CONCLUSIONS'
      element.content = 'Brief Conclusions'
    end
  end

  sib = doc.css('hr')[1]
  result = []
  loop do
    sib = sib.next_element
    break if sib.name == 'hr'
    result << sib.dup
  end
  result
end

def extract_image_urls(doc)
  doc.css('img').map { |element| element.attribute('src') }
end

def translate_notes(body, notes)
  body.each do |element|
    next unless element.name == 'p'
    element.css('sup[class="anote"]').each do |note|
      note.replace('<span class="fn">' + notes.pop.map(&:to_html).join("\n") +
                   '</span>')
    end
  end
end

url_fragments = (1..43).map { |n| "pe-ch%02d" % n }
url_fragments << 'conclusion'
base_url = 'https://www.marxists.org/subject/economy/authors/pe'
urls = url_fragments.map { |f| "#{base_url}/#{f}.htm" }

output = File.open('scrape-output.html', 'w')
image_urls_output = File.open('image-urls.txt', 'w')

urls.each do |url|
  puts "Scraping \"#{url}\"..."
  html = open(url)

  # Fix an HTML typo in chapter 16:
  html.sub!('<.p>', '</p>')

  doc = Nokogiri::HTML(html, nil, 'utf-8')

  notes = extract_notes(doc)
  num, title = *extract_chapter_title(doc)
  body = extract_body(doc)
  image_urls = extract_image_urls(doc)
  translate_notes(body, notes)
  output.write <<-HTML
    <section class="chapter">
      <header>
        #{num ? "<h1>Chapter #{num}</h1>" : ''}
        <h2 title="#{title}">#{title}</h2>
      </header>
      #{body.map(&:to_html).join("\n")}
    </section>
  HTML
  image_urls.each { |url| image_urls_output.write(url.to_s + "\n") }
end

output.close

# `books`

Quick and dirty project for converting HTML-based books into PDFs for printing.

## Dependencies

* Ruby 2.3 (or higher)
* [Sass](https://sass-lang.com/install) (standalone), for generating stylesheets
* [Prince 12](https://www.princexml.com/download/) (for non-commercial use), for generating PDFs from HTML/CSS
* [QPDF](http://qpdf.sourceforge.net/) (`apt-get install qpdf`), for manipulating the generated PDFs to remove the Prince watermark
* [Junicode](https://www.fontsquirrel.com/fonts/junicode) (font)

Additionally, `scrape.rb` scripts require `nokogiri` (`gem install nokogiri`) for parsing HTML.

## Execution

```sh
./build foundations-of-leninism book
./build foundations-of-leninism cover
```

# Runs on Ruby 2.3
#
# `nokogiri` gem must be installed

require 'nokogiri'
require 'net/http'
require 'uri'

def open(url)
  Net::HTTP.get(URI.parse(url))
end

def titleize(str)
  str = str.capitalize
  words_no_cap = %w(a an and at but by for in nor of on or so the to up yet)
  phrase = str.split(' ').map { |word|
    words_no_cap.include?(word) ? word : word.capitalize
  }.join(' ')
  phrase
end

def extract_chapter_title(doc)
  sib = doc.at_css('hr')
  loop do
    sib = sib.next_element
    if sib.name == 'h5'
      return titleize(sib.inner_text)
    elsif sib.name == 'hr'
      return nil
    end
  end
end

def extract_body(doc)
  # Pre-process doc.
  doc.css('p').each do |element|
    if element.attribute('class').to_s.include?('quote')
      element.set_attribute('class', 'quote')
    elsif element.attribute('class').to_s == 'fst'
      element.set_attribute('class', 'no-indent')
    else
      element.remove_attribute('class')
    end
  end

  doc.css('h4 a').remove
  doc.css('h4 span').each do |element|
    element.remove_attribute('class')
  end
  doc.css('h4').each do |element|
    element.inner_html = titleize(element.inner_text)
  end

  sib = doc.css('hr')[1]
  result = []
  loop do
    sib = sib.next_element
    break if sib.name == 'hr'
    result << sib.dup
  end
  result
end

url_fragments = (1..14).map { |n| "ch%02d" % n }
url_fragments.unshift('intro')
base_url = 'https://www.marxists.org/reference/archive/stalin/works/1939/x01'
urls = url_fragments.map { |f| "#{base_url}/#{f}.htm" }

output = File.open('scrape-output.html', 'w')

urls.each do |url|
  puts "Scraping \"#{url}\"..."
  html = open(url)

  doc = Nokogiri::HTML(html, nil, 'iso-8859-1')

  title = extract_chapter_title(doc)
  body = extract_body(doc)
  output.write <<-HTML
    <section class="chapter" title="#{title}">
      <header>
        <h1></h1>
        <h2>#{title}</h2>
      </header>
      #{body.map(&:to_html).join("\n")}
    </section>
  HTML
end

output.close
